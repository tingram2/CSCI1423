#!usr/bin/perl

#running a global substitution regular expression
#s stands for substitution, l is the letter we are looking for, y is the letter that will replace l, and g makes it replace all l's (global)
#=~ binding operator
$foo = "jelly";
$foo =~ s/l/y/g;
print "$foo \n";

#Running a matching regular expression on default variable which is initialized by a foreach loop
#qw stands for quote word
@bar = qw(cat hat dog cheese monkey keys whatever tape);
foreach $word (@bar) {
	if ($word =~ m/at/) {
	print "We found the letters a and t next to each other in the word $word\n";	
}
}
