#!/usr/bin/perl
use warnings ;

print "A number please: ";
my $number = <STDIN>;
chomp $number;

print "Another number please: ";
my $lumber = <STDIN>;
chomp $lumber;

print "$number + $lumber = ";
print $number + $lumber;
print "\n";

print "$number - $lumber = ";
print $number - $lumber;
print "\n";

print "$number * $lumber = ";
print $number * $lumber;
print "\n";

print "$number / $lumber = ";
print $number / $lumber;
print "\n";

print "$number % $lumber = ";
print $number % $lumber;
print "\n";
