#!/usr/bin/perl
use warnings ;

print "Give me an adjective please: ";
my $adjective = <STDIN>;
chomp $adjective;

print "Give me a noun: ";
my $noun = <STDIN>;
chomp $noun;

print "Now give me a verb: ";
my $verb = <STDIN>;
chomp $verb;

print "Give me a name: ";
my $name = <STDIN>;
chomp $name;

print "Lastly, give me another noun: ";
my $lastnoun = <STDIN>;
chomp $lastnoun;

print "There was a(n) $adjective dog that ran out of the $noun. While it ran, a person took a $verb beside it. The person's name was $name, and $name owned a spaceship. The dog and $name took flight and landed on the $lastnoun.\n"
