#!/usr/bin/perl

use warnings ;

@pezzy;
print "The pez dispenser is empty.\n";
print "@pezzy\n";

while (@pezzy < 10){
	my $num = <STDIN>;
	chomp $num;
	push @pezzy, $num;
}
print "Let's load it. Type a number and press enter. Repeat nine more times please: \n";

$number1 = <STDIN>;
chomp $number1;

$number2 = <STDIN>;
chomp $number2;

$number3 = <STDIN>;
chomp $number3;

$number4 = <STDIN>;
chomp $number4;

$number5 = <STDIN>;
chomp $number5;

$number6 = <STDIN>;
chomp $number6;

$number7 = <STDIN>;
chomp $number7;

$number8 = <STDIN>;
chomp $number8;

$number9 = <STDIN>;
chomp $number9;

$number10 = <STDIN>;
chomp $number10;

@pezzy = ($number1, $number2, $number3, $number4, $number5, $number6, $number7, $number8, $number9, $number10);
print "$number1 $number2 $number3 $number4 $number5 $number6 $number7 $number8 $number9 $number10\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

pop (@pezzy);
print "@pezzy\n";

print "Now the pez is empty.\n";
