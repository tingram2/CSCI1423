#!usr/bin/perl

#use strict;
use warnings;
open(HTML, '>', 'foo.html') || die ("could not find file: $! \n"); 
open(DATA, '<', 'data.csv') || die ("could not find file: $! \n");
my $line;

print HTML <<ENDTAG;
<html>
	<head>Here is the heading.
	<title>CSV FILE</title>
	<style>table,td{
		border:solid 5px #000000;
	}
	</style>
	</head>
	<body>
	<p>Here is the CSV file in an HTML table:</p>
	<table>
ENDTAG

while ($line = <DATA>){  
	my @cells= split ',',$line;
	print HTML '<tr>', '<td>', @cells, '</td>', '</tr>';
}

print HTML <<ENDTAG;
	</table>
	</body>
</html>
ENDTAG
