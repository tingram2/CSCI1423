#!/usr/bin/perl
use warnings ; 

# Stack

@bop = (1, 2, 3, 4);
push (@bop,5);
print "@bop\n";

pop (@bop);
print "@bop\n";

# Queue

shift (@bop);
print "@bop\n";

