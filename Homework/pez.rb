#!usr/bin/ruby

pez = []
puts "Here is the empty pez dispenser: #{pez}"
puts "Add to the dispenser 10 times"

10.times do
  pez << gets.chomp
end

puts "#{pez}"

until pez.empty? do
  puts "Popping #{pez.last} out of the pez dispenser"
  pez.pop
end

puts "The pez dispenser is now empty again #{pez}"
