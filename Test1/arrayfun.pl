#!usr/bin/perl

@foo = (1, 2, 3, 4, 5);
print "@foo\n";

shift (@foo);
print "@foo\n";

pop (@foo);

push (@foo,20);
print "@foo\n";

@foo[2] = "55";
print "@foo\n";
