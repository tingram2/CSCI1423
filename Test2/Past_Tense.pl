#!/usr/bin/perl

open(VERBS, '<', 'verbs.txt') || die("could not find file: $! \n");
chomp(@verbs = <VERBS>);
print "@verbs\n";
foreach (@verbs){
	s/ing/ed/g;
	print "@verbs\n";
}
open(PAST, '>', 'past_tenseverbs.txt') || die("could not find file: $! \n");
print PAST "@verbs";
