#!usr/bin/perl

# While loop
$baz = 2;
while($baz < 6){
	print "$baz \n";
	$baz++;
}
print "\$baz is $baz \n";

# Do loop
do {
	print "$baz \n";
	$baz--;
} while ($baz > 2);
print "\$baz is $baz \n";
