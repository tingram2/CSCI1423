#!/usr/bin/perl

#use warnings;
#use diagnostics;
use Cwd;
use Time::localtime;
#be aware of this library which is where I am now
use lib '.';
use Foo;

print cwd() . "\n";

print ctime() . "\n";

foo();
bar("candy", "bar");
$sum = baz(2, 3);
print "2 + 3 is $sum \n";
