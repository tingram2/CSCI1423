#!/usr/bin/perl
use warnings;
print "A number please: \n";
chomp($num1 = <>);

print "Another number please: \n";
chomp($num2 = <>);

sub sum {
	print "The sum is:\n";
	$num1 + $num2; 
}
$sum = sum;
print "$sum.\n";

sub difference {
	print "The difference is:\n";
	$num1 - $num2;
}
$dif = difference;
print "$dif.\n";

sub product {
	print "The product is:\n";
	$num1 * $num2;
}
$pro = product; 
print "$pro.\n";

sub quotient {
	print "The quotient is:\n";
	$num1 / $num2;
}
$quo = quotient;
print "$quo.\n";
 
sub remainder {
	print "The remainder is:\n";
	$num1 % $num2;
}
$rem = remainder;
print "$rem.\n";
